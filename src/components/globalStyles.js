import { createGlobalStyle } from "styled-components"

export const GlobalStyle = createGlobalStyle`
  *,
  ::after,
  ::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
    text-decoration: none;
  }
    
  body {
    box-sizing: border-box;
    font-family: 'Abril Fatface', cursive;
    margin: 0 auto;
    width: 100%;
    max-width: 1920px;
      background-color: #232526;  /* fallback for old browsers */


  }
  a {
    color: #fff;
    text-decoration: underline;
    &:visited {
      color: #fff;
    }
  }
  img {
    width: 100%;
  }
  h1, h2, h3 {
    font-weight: 400;
    letter-spacing: 0.5px;
    color: #fff;
  }
  h3 {
    text-align: center;
  }
  h1 {
    font-size: 2.7rem;
    line-height: 1;
    padding: 0 7px;
  }
  h2 {
    font-size: 2.6rem;
  }
  p {
    padding: 10px;
    font-family: 'Montserrat', sans-serif;
    text-align: center;
    color: #fff;
  }
  ul, li {
    list-style: none;
  }
  main {
    width: 100%;
    display:flex;
    max-width: 1920px;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    margin: 40px 0;
  }
  form {
    font-family: 'Montserrat', sans-serif;
    display: flex;
    flex-direction: column;
    max-width: 370px;
    width: 100%;
    padding: 20px 10px;
    transition: 0.5s all ease;
  }
  input[type=text], input[type=email] {
  width: 100%;
  color: #fff;
  padding: 12px 10px;
  font-size: 1rem;
  margin: 8px 0;
  box-sizing: border-box;
  border: 2px solid #fff;
  border-radius: 4px;
  background-color: #232526;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  outline: none;
  transition: 0.5s all ease;
}

input[type=text]:focus, input[type=email]:focus {
  border-color: #8e9eab;
}

textarea {
  width: 100%;
  color: #fff;
  padding: 12px 10px;
  font-size: 1rem;
  margin: 8px 0;
  box-sizing: border-box;
  border: 2px solid #fff;
  border-radius: 4px;
  background-color: #232526;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  outline: none;
  resize: none;
  height: 150px;
  font-family: inherit;
}

input[type=submit], input[type=button] {
  background-color: #fff;
  color: #232526;
  border-radius: 5px;
  max-width: 100px;
  padding: 10px 20px;
  outline: none;
  border: 0;
  font-weight: 800;
  font-family: 'Montserrat', sans-serif;
  margin-top: 10px;
  transition: 0.2s all ease;
  border: 1px solid #232526;
}
input[type=submit]:hover, input[type=button]:hover {
  background-color:#232526;
  color: #fff;
  border: 1px solid #fff;
  cursor: pointer;
}
lMontserrat {
  color: #fff;
  padding-top:10px;
}
  .hero {
    display: flex;
    width: 100%;
    max-width: 370px;
    flex-flow: row wrap;
    justify-content: center;
    align-items: center;
  }
  .text-container{
    transform: rotate(-90deg);
  }
  .sar {
    width: 100%;
    display: flex;
  }
  .and {
    width: 119%;
    line-height: 0.7;
    text-align: right;
  }
  .textbox {
    max-width: 370px;
    padding: 20px 10px;
  }
  .partner {
    transition: 0.5s ease all;
  }
  .hero-img {
    max-width: 189px;
    width: 100%;
  }
  
section {
  background-image: radial-gradient(farthest-side at 50% 85%, #232526, #232526);
 display: content;
  overflow: hidden;
  color: #f5f5f5;
  line-height: 1.34;
  padding-block: 20px;
}
#timer {
  font-family: Montserrat;
  font-size: 2rem;
  opacity: 0.25;
  text-align: right;
  width: 25%;
}
#timer small {
  font-size: 1rem;
  display: block;
}
#countdown {
  display: flex;
  flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    gap: 10px;
}
#countdown .counter {
  width: 192px;
  position: relative;
  font-family: Montserrat;
  font-size: 108px;
  text-transform: uppercase;
  text-align: center;
  line-height: 1;
  display: grid;
  @media (max-width: 400px) {
    width: 134px;
  }
}
#countdown .counter span::before {
  content: attr(b);
}
#countdown .counter span.ping::before {
  animation: pingEffect 0.25s 1 forwards;
}
#countdown .counter b {
  font-size: 14px;
  width: 100%;
}
@-moz-keyframes pingEffect {
  to {
    text-shadow: 0 0 12px rgba(238,238,238,0.5), 0 0 32px rgba(238,238,238,0.3), 0 24px 48px rgba(238,238,238,0.1), 0 -24px 48px rgba(238,238,238,0.1);
  }
}
@-webkit-keyframes pingEffect {
  to {
    text-shadow: 0 0 12px rgba(238,238,238,0.5), 0 0 32px rgba(238,238,238,0.3), 0 24px 48px rgba(238,238,238,0.1), 0 -24px 48px rgba(238,238,238,0.1);
  }
}
@-o-keyframes pingEffect {
  to {
    text-shadow: 0 0 12px rgba(238,238,238,0.5), 0 0 32px rgba(238,238,238,0.3), 0 24px 48px rgba(238,238,238,0.1), 0 -24px 48px rgba(238,238,238,0.1);
  }
}
@keyframes pingEffect {
  to {
    text-shadow: 0 0 12px rgba(238,238,238,0.5), 0 0 32px rgba(238,238,238,0.3), 0 24px 48px rgba(238,238,238,0.1), 0 -24px 48px rgba(238,238,238,0.1);
  }
}





`
