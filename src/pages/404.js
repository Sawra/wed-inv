import React from "react"

const ErrorPage = () => (
  <main>
    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn't exist... the sadness.</p>
  </main>
)

export default ErrorPage
