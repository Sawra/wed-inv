import React from "react"
import { GlobalStyle } from "../components/globalStyles"

export default function Thanks() {   
return (
<main>
    <GlobalStyle />
    <h1>Tack så hjärtligt!</h1>
    <p>Ditt svar är skickat!</p>
  </main>
)
}
