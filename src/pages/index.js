import React from "react"
import { GlobalStyle } from "../components/globalStyles"
import { graphql } from "gatsby"
import { Helmet } from "react-helmet"
import Img from "gatsby-image"
import { Animated } from "react-animated-css"
import Toastpar from "../assets/toastpar.jpg"

export default function Home({ data }) {
  const gc = s => document.querySelector('#countdown [count="' + s + '"] span')

  let d = new Date("2022-07-30T00:15:00") - new Date()

  const mainCalc = (s, t, c) => {
    gc(s).classList.remove("ping")
    const m = t % c
    const e = a => gc(s)[a + "Attribute"].bind(gc(s))
    e("set")("b", m < 10 ? "0" + m : m)
    setTimeout(() => gc(s).classList.add("ping"), 10)
    return m
  }

  const count = (b = 0) => (d -= 1000) && count.seconds(d, b)
  const opti = (v, n) => (v - (v % n)) / n

  count.seconds = (t, i = !1) => {
    t = opti(t, 1000)
    i && count.minutes(t, i)
    if (mainCalc("seconds", t, 60) === 59) count.minutes(t, i)
  }
  count.minutes = (t, i = !1) => {
    t = opti(t, 60)
    i && count.hours(t, i)
    if (mainCalc("minutes", t, 60) === 59) count.hours(t, i)
  }
  count.hours = (t, i = !1) => {
    t = opti(t, 60) - 1
    i && count.days(t)
    if (mainCalc("hours", t, 24) === 23) count.days(t)
  }
  count.days = t => {
    t = opti(t, 24)
    gc("days").setAttribute("b", t < 10 ? "0" + t : t)
    setTimeout(() => gc("days").classList.add("ping"), 10)
  }

  setTimeout(() => {
    count(true)
    setInterval(count, 1000)
  }, d % 1000)

  return (
    <main>
      <Helmet>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"
        ></link>
      </Helmet>
      <GlobalStyle />
      <div className="hero">
        <div className="text-container">
          <Animated
            animationIn="fadeInRight"
            animationOut="flash"
            animationInDuration={1000}
            animationOutDuration={1000}
            isVisible={true}
          >
            <div className="sar">
              <h2>Sara</h2>
              <h1>&amp;</h1>
            </div>
            <h2 className="and">Andreas</h2>
          </Animated>
        </div>
        <div className="hero-img">
          <Img fluid={data.file.childImageSharp.fluid} />
        </div>
      </div>
      <section className="timer-container">
        <span id="timer" />
        <div id="countdown">
          <div class="counter" count="days">
            <span b="--"></span>
            <b>Dagar</b>
          </div>
          <div class="counter" count="hours">
            <span b="--"></span>
            <b>Timmar</b>
          </div>
          <div class="counter" count="minutes">
            <span b="--"></span>
            <b>Minuter</b>
          </div>
          <div class="counter" count="seconds">
            <span b="--"></span>
            <b>Sekunder</b>
          </div>
        </div>
      </section>
      <div className="textbox">
        <p>
          Välkommen på det mest efterlängtade bröllopet i modern tid! Lördagen
          den 30 Juli 2022 kommer Sara och Andreas ställa till med fest när vi
          äntligen säger JA till varandra.
          <br />
        </p>
        <h3>Vart? 🧭</h3>
        <p>
          Klockan 15.00 kommer vigseln att hållas på gräsmattan bakom
          festlokalen Örlogskapellet, vi ser gärna att ni är där{" "}
          <strong>senast 14:45</strong> Barn är välkomna till vigseln, men vi
          ser gärna att de stannar hemma under kvällen. Det finns ett antal
          parkeringsplaster på området, om du önskar en parkering så meddela oss
          i god tid innan. Om ni tar kollektivtrafik så kan ni se på den här
          länken vart{" "}
          <a
            href="https://www.google.com/maps/place/Restaurang+%C3%96rlogskapellet/@57.6803506,11.8881705,17.25z/data=!4m5!3m4!1s0x464f8cd9bc279215:0xc96d02ffd14398d3!8m2!3d57.680158!4d11.8883269"
            target="_blank"
            rel="noopener noreferrer"
          >
            Örlogskapellet
          </a>{" "}
          ligger.
          <br />
          <br />
        </p>
        <h3>Klädkod för kvällen 💃🕺</h3>
        <p>Kavaj</p>
        <br />
        <h3>Bröllopsgåvor 💐</h3>
        <p>
          Bröllopsgåvor är alltid välkommet om man vill, vi önskar oss ett
          bidrag till vår bröllopsresa. Det går jättebra att swisha till Andreas
          på nummer <strong>0702-95 83 54</strong>
          <br />
          <br />
        </p>
        <h3>Toastparet 🥂</h3>
        <p>
          Vi vill också passa på att presentera vårt toastpar för dagen; Fredrik
          och Erik!
          <br />
          Dessa två herrar kan man höra av sig till om man önskar hålla tal,
          sjunga en liten trudelutt, göra en lek, visa ett oskämmigt bildspel
          eller något annat under festens gång. <br />
          <br />
          <strong>Anmälan till tal stänger den 20 Juli</strong> <br />
          <br />
          <a href="mailto:eric.wensberg@gmail.com">Maila Erik</a> eller{" "}
          <a href="mailto:fredrik.arousell@dbschenker.com">maila Fredrik</a>
        </p>{" "}
        <br />
        <img alt="toastpar" src={Toastpar} />
        <br />
        <br />
      </div>
    </main>
  )
}

export const query = graphql`
  query {
    file(relativePath: { eq: "saranad.jpg" }) {
      childImageSharp {
        # Specify the image processing specifications right in the query.
        # Makes it trivial to update as your page's design changes.
        fluid(maxWidth: 900) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
