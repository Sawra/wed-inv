module.exports = {
  siteMetadata: {
    title: `Gatsby Boilerplate`,
    description: `Boilerplate to get your Gatsby up and running in no time.`,
    author: `Joakim Wretlind`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `assets`,
        path: `${__dirname}/src/assets`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `minimal-ui`,
        icon: `src/assets/party-popper.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts-v2`,
      options: {
        fonts: [
          {
            family: "Abril Fatface",
            weights: ["400"],
          },
          {
            family: "Montserrat",
            weights: ["300", "400"],
          },
        ],
      },
    },
    `gatsby-plugin-styled-components`,
  ],
}
