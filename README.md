[![Netlify Status](https://api.netlify.com/api/v1/badges/6f1daa3b-2ed4-400e-9ec3-7f1be074d7b7/deploy-status)](https://app.netlify.com/sites/gallant-brahmagupta-b472fe/deploys)

<div>
* You might have to run npm update to make helmet and filesystem to work. <br/>
* In the gatsby-plugin-manifest, icon has to be there. If you use another folder for your icon, make sure to change it in the gatsby.config.js <br/>
  </div>
